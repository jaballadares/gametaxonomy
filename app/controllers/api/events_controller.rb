class Api::EventsController < ApplicationController
  respond_to :json

  def index
    events = Event.sorted

    respond_with events, location: api_events_url
  end

  def show
    event = Event.find params[:id]

    respond_with event, location: api_event_url(event)
  end

  def create
    name = event_params.delete :name

    event = Event.where(name: name).first_or_initialize
    event.update event_params

    respond_with event, location: api_event_url(event)
  end

  private

  def event_params
    params.require(:event).permit(:name, :event_date, :preview_countdown,
      :game_time_limit)
  end
end
