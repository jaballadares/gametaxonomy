class Api::PlayersController < ApplicationController
  respond_to :json, :csv

  def index
    players = Player.sorted
    players = players.where(event_id: params[:event_id]) if params[:event_id]

    respond_with players, location: api_players_url
  end

  def create
    player = Player.new player_params
    player.save

    respond_with player, location: api_players_url
  end

  private

  def player_params
    params.require(:player).permit(:event_id, :medical_concept_id, :first_name,
      :last_name, :score_correct, :score_wrong, :total_terms, :game_time, :email)
  end
end
