class Api::MedicalConceptsController < ApplicationController
  respond_to :json

  def index
    medical_concepts = MedicalConcept.sorted

    respond_with medical_concepts, location: api_medical_concepts_url
  end
end
