require "csv"

class PlayerToCsv
  attr_reader :players

  def initialize(players)
    @players = players
  end

  def call(options)
    CSV.generate(options) do |csv|
      csv << csv_headers
      players.all.each do |player|
        csv << values(player)
      end
    end
  end

  def csv_headers
    ["First Name", "Last Name", "Email", "Created at", "Score Correct", "Score Wrong", "Total"]
  end

  def values(player)
    [player.first_name, player.last_name, player.email, player.created_at, player.score_correct, player.score_wrong, player.total_terms]
  end
end
