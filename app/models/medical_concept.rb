class MedicalConcept < ActiveRecord::Base
  has_many :concepts, dependent: :destroy
  has_many :terms, dependent: :destroy

  validates :name,
    presence: true

  scope :sorted, -> { order(name: :asc) }

  def as_json(options = {})
    except = [:created_at, :updated_at]
    super include: { terms: { methods: [:concept_name], except: except }, concepts: { methods: [:pos_class], except: except } },
      except: except
  end
end
