class Concept < ActiveRecord::Base
  enum kind: %w(concept_attributes concept_relationships)

  belongs_to :medical_concept

  validates :name, :medical_concept_id,
    presence: true

  scope :sorted, -> { order(position: :asc) }

  def pos_class
    concept_relationships? ? "r" : "a"
  end
end
