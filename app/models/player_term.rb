class PlayerTerm < ActiveRecord::Base
  belongs_to :player
  belongs_to :term

  validates :player_id, :term_id,
    presence: true
end
