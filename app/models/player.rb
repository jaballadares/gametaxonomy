class Player < ActiveRecord::Base
  belongs_to :event
  belongs_to :medical_concept

  validates :first_name, :last_name, :event_id, :medical_concept_id,
    presence: true

  validates :score_correct, :score_wrong,
    numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  scope :sorted, -> { order("(players.score_correct / players.total_terms::float) DESC, players.game_time ASC") }
  scope :includes_event, -> { includes(:event) }
  scope :oldest_first, -> { order(created_at: :asc) }

  def as_json(options = {})
    super.merge event_name: event_name,
      event_date: event_event_date.strftime("%m/%d/%Y"),
      game_time: [game_time / 60, game_time % 60].join(":"),
      rank: rank
  end

  def rank
    player_ids = event.players.sorted.pluck(:id)
    player_ids.index(id) + 1
  end

  delegate :name, :event_date, to: :event, prefix: true

  def self.to_csv(options = {})
    player_to_csv = PlayerToCsv.new oldest_first
    player_to_csv.call options
  end
end
