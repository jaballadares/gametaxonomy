class Term < ActiveRecord::Base
  belongs_to :medical_concept
  belongs_to :concept

  validates :name, :medical_concept, :concept,
    presence: true

  delegate :name, to: :concept, prefix: true
end
