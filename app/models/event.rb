class Event < ActiveRecord::Base
  has_many :players, dependent: :destroy

  validates :name, :event_date, :preview_countdown, :game_time_limit,
    presence: true

  validates :preview_countdown, :game_time_limit,
    numericality: { only_integer: true, greater_than: 0 }

  scope :sorted, -> { order(event_date: :desc) }

  def as_json(options = {})
    super.merge event_date: (event_date && event_date.strftime("%m/%d/%Y"))
  end
end
