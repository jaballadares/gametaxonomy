require "csv"

desc "Import Medical Concepts"
task :import_medical_concepts => :environment do
  csv_file = "#{Rails.root.to_s}/db/medical_concepts.csv"
  CSV.foreach(csv_file, headers: true) do |row|
    raw_data = row.to_hash

    medical_concept = MedicalConcept.
      where(name: raw_data["Medical Concept"].strip).
      first_or_create

    next_position = if raw_data["Concept Type"].strip == "concept_relationships"
      medical_concept.concepts.concept_relationships.count + 1
    else
      medical_concept.concepts.concept_attributes.count + 1
    end

    concept = medical_concept.concepts.create(name: raw_data["Concept"].strip,
      kind: raw_data["Concept Type"].strip.to_sym,
      position: next_position)

    term = medical_concept.terms.
      where(name: raw_data["Term"].strip, concept_id: concept.id).
      first_or_create

    puts "."
  end

  puts "Finished!"
end
