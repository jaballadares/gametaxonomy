Rails.application.routes.draw do
  root "angular#show"

  resource :angular, only: :show

  namespace :api do
    resources :events, only: [:index, :show, :create]
    resources :players, only: [:index, :create]
    resources :medical_concepts, only: :index
  end

  get "/*path", to: "angular#show"
end
