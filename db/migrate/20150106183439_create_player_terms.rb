class CreatePlayerTerms < ActiveRecord::Migration
  def change
    create_table :player_terms do |t|
      t.belongs_to :term, index: true
      t.belongs_to :concept, index: true

      t.timestamps
    end
  end
end
