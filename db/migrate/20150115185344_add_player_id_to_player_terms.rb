class AddPlayerIdToPlayerTerms < ActiveRecord::Migration
  def change
    add_column :player_terms, :player_id, :integer
  end
end
