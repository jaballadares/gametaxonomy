class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.belongs_to :event, index: true
      t.belongs_to :medical_concept, index: true

      t.string :first_name
      t.string :last_name
      t.date :event_date
      t.integer :score_correct
      t.integer :score_wrong
      t.integer :game_time

      t.timestamps
    end
  end
end
