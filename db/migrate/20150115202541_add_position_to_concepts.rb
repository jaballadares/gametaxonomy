class AddPositionToConcepts < ActiveRecord::Migration
  def change
    add_column :concepts, :position, :integer, default: 0
  end
end
