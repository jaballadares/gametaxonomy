class CreateMedicalConcepts < ActiveRecord::Migration
  def change
    create_table :medical_concepts do |t|
      t.string :name

      t.timestamps
    end
  end
end
