class CreateConcepts < ActiveRecord::Migration
  def change
    create_table :concepts do |t|
      t.belongs_to :medical_concept, index: true

      t.string :name
      t.integer :kind #(attributes, relationships)

      t.timestamps
    end
  end
end
