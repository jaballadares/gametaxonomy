class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.date :event_date
      t.integer :preview_countdown
      t.integer :game_time_limit

      t.timestamps
    end
  end
end
