class AddTotalTemsToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :total_terms, :integer
  end
end
