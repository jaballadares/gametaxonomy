class CreateTerms < ActiveRecord::Migration
  def change
    create_table :terms do |t|
      t.belongs_to :medial_concept, index: true
      t.belongs_to :concept, index: true

      t.string :name

      t.timestamps
    end
  end
end
