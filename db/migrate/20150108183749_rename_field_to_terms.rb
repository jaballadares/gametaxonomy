class RenameFieldToTerms < ActiveRecord::Migration
  def up
    rename_column :terms, :medial_concept_id, :medical_concept_id
  end

  def down
    rename_column :terms, :medical_concept_id, :medial_concept_id
  end
end
