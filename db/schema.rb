# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150223223212) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "concepts", force: true do |t|
    t.integer  "medical_concept_id"
    t.string   "name"
    t.integer  "kind"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",           default: 0
  end

  add_index "concepts", ["medical_concept_id"], name: "index_concepts_on_medical_concept_id", using: :btree

  create_table "events", force: true do |t|
    t.string   "name"
    t.date     "event_date"
    t.integer  "preview_countdown"
    t.integer  "game_time_limit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "medical_concepts", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "player_terms", force: true do |t|
    t.integer  "term_id"
    t.integer  "concept_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "player_id"
  end

  add_index "player_terms", ["concept_id"], name: "index_player_terms_on_concept_id", using: :btree
  add_index "player_terms", ["term_id"], name: "index_player_terms_on_term_id", using: :btree

  create_table "players", force: true do |t|
    t.integer  "event_id"
    t.integer  "medical_concept_id"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "event_date"
    t.integer  "score_correct"
    t.integer  "score_wrong"
    t.integer  "game_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "total_terms"
    t.string   "email"
  end

  add_index "players", ["event_id"], name: "index_players_on_event_id", using: :btree
  add_index "players", ["medical_concept_id"], name: "index_players_on_medical_concept_id", using: :btree

  create_table "terms", force: true do |t|
    t.integer  "medical_concept_id"
    t.integer  "concept_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terms", ["concept_id"], name: "index_terms_on_concept_id", using: :btree
  add_index "terms", ["medical_concept_id"], name: "index_terms_on_medical_concept_id", using: :btree

end
